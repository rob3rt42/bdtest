-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: roberto
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `artefactocfdi`
--

DROP TABLE IF EXISTS `artefactocfdi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `artefactocfdi` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `IdTipo` int DEFAULT NULL,
  `Clave` varchar(45) DEFAULT NULL,
  `Descripcion` varchar(140) DEFAULT NULL,
  `AplicaciontipoPersona` int DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artefactocfdi`
--

LOCK TABLES `artefactocfdi` WRITE;
/*!40000 ALTER TABLE `artefactocfdi` DISABLE KEYS */;
INSERT INTO `artefactocfdi` VALUES (1,12,'601','General de Ley Personas Morales',15),(2,12,'603','Personas Morales con Fines no Lucrativos',15),(3,12,'605','Sueldos y Salarios e Ingresos Asimilados a Salarios',14),(4,12,'606','Arrendamiento',14),(5,12,'607','Régimen de Enajenación o Adquisición de Bienes',14),(6,12,'608','Demás ingresos',14),(7,12,'610','Residentes en el Extranjero sin Establecimiento Permanente en México',16),(8,12,'611','Ingresos por Dividendos (socios y accionistas)',14),(9,12,'612','Personas Físicas con Actividades Empresariales y Profesionales',14),(10,12,'614','Ingresos por intereses',14),(11,12,'615','Régimen de los ingresos por obtención de premios',14),(12,12,'616','Sin obligaciones fiscales',14),(13,12,'620','Sociedades Cooperativas de Producción que optan por diferir sus ingresos',15),(14,12,'621','Incorporación Fiscal',14),(15,12,'622','Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras',15),(16,12,'623','Opcional para Grupos de Sociedades',15),(17,12,'624','Coordinados',15),(18,12,'625','Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas',14),(19,12,'626','Régimen Simplificado de Confianza',16),(20,13,'G01','Adquisición de mercancías.',16),(21,13,'G02','Devoluciones, descuentos o bonificaciones.',16),(22,13,'G03','Gastos en general.',16),(23,13,'I01','Construcciones.',16),(24,13,'I02','Mobiliario y equipo de oficina por inversiones.',16),(25,13,'I03','Equipo de transporte.',16),(26,13,'I04','Equipo de computo y accesorios.',16),(27,13,'I05','Dados, troqueles, moldes, matrices y herramental.',16),(28,13,'I06','Comunicaciones telefónicas.',16),(29,13,'I07','Comunicaciones satelitales.',16),(30,13,'I08','Otra maquinaria y equipo.',16),(31,13,'D01','Honorarios médicos, dentales y gastos hospitalarios.',14),(32,13,'D02','Gastos médicos por incapacidad o discapacidad.',14),(33,13,'D03','Gastos funerales.',14),(34,13,'D04','Donativos.',14),(35,13,'D05','Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).',14),(36,13,'D06','Aportaciones voluntarias al SAR.',14),(37,13,'D07','Primas por seguros de gastos médicos.',14),(38,13,'D08','Gastos de transportación escolar obligatoria.',14),(39,13,'D09','Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.',14),(40,13,'D10','Pagos por servicios educativos (colegiaturas).',14),(41,13,'S01','Sin efectos fiscales.  ',16),(42,13,'CP01','Pagos',16),(43,13,'CN01','Nómina',14);
/*!40000 ALTER TABLE `artefactocfdi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clasificacion`
--

DROP TABLE IF EXISTS `clasificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clasificacion` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Grupo` varchar(45) DEFAULT NULL,
  ` Codigo` varchar(45) DEFAULT NULL,
  `Nombre` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clasificacion`
--

LOCK TABLES `clasificacion` WRITE;
/*!40000 ALTER TABLE `clasificacion` DISABLE KEYS */;
INSERT INTO `clasificacion` VALUES (1,'1','1','Efectivo'),(2,'1','2','Cheque Nominativo'),(3,'1','3','Transferencia electrónica de fondos'),(4,'1','4','Tarjeta de crédito'),(5,'1','5','Monedero electrónico'),(6,'1','6','Dinero electrónico'),(7,'1','8','Vales de despensa'),(8,'1','12','Dación en pago'),(9,'1','13','Pago por subrogación'),(10,'1','14','Pago por consignación'),(11,'1','15','Condonación'),(12,'1','17','Compensación'),(13,'1','23','Novación'),(14,'1','24','Confusión'),(15,'1','25','Remisión de deuda'),(16,'1','26','Prescripción o caducidad'),(17,'1','27','A satisfacción del acreedor'),(18,'1','28','Tarjeta de débito'),(19,'1','29','Tarjeta de servicios'),(20,'1','30','Aplicación de anticipos'),(21,'1','31','Intermediario pagos'),(22,'1','99','Por definir'),(23,'4','I','Ingreso'),(24,'4','E','Egreso'),(25,'4','T','Traslado'),(26,'4','N','Nómina'),(27,'4','P','Pago'),(28,'5','1','No Aplica'),(29,'5','2','Definitva'),(30,'5','3','Temporal'),(31,'6','PUE','Pago en una sola exhibición'),(32,'6','PPD','Pago en parcialidades o diferido'),(33,'7','1','Diario'),(34,'7','2','Semanal'),(35,'7','3','Quincenal'),(36,'7','4','Mensual'),(37,'7','5','Bimestral'),(38,'8','1','Nota de crédito de los documentos relacionados'),(39,'8','2','Nota de débito de los documentos relacionados'),(40,'8','3','Devolución de mercancía sobre facturas o traslados previos'),(41,'8','4','Sustitución de los CFDI previos'),(42,'8','5','Traslados de mercancías facturados previamente'),(43,'8','6','Factura generada por los traslados previos'),(44,'8','7','CFDI por aplicación de anticipo'),(45,'13','64','Libra por pulgada cuadrada, calibre'),(46,'13','66','Oersted'),(47,'13','76','Gauss'),(48,'13','78','Kilogauss'),(49,'13','84','Kilopound-force por pulgada cuadrada'),(50,'14','1','No objeto de impuesto.'),(51,'14','2','Sí objeto de impuesto.'),(52,'14','3','Sí objeto del impuesto y no obligado al desglose.'),(53,'16','1','ISR'),(54,'16','2','IVA'),(55,'16','3','IEPS'),(56,'18','1','ACAPULCO, ACAPULCO DE JUAREZ, GUERRERO.'),(57,'18','2','AGUA PRIETA, AGUA PRIETA, SONORA.'),(58,'18','5','SUBTENIENTE LOPEZ, SUBTENIENTE LOPEZ, QUINTANA ROO.'),(59,'18','6','CIUDAD DEL CARMEN, CIUDAD DEL CARMEN, CAMPECHE.'),(60,'18','7','CIUDAD JUAREZ, CIUDAD JUAREZ, CHIHUAHUA.'),(61,'18','8','COATZACOALCOS, COATZACOALCOS, VERACRUZ.'),(62,'18','11','ENSENADA, ENSENADA, BAJA CALIFORNIA.'),(63,'18','12','GUAYMAS, GUAYMAS, SONORA.'),(64,'18','14','LA PAZ, LA PAZ, BAJA CALIFORNIA SUR.'),(65,'18','16','MANZANILLO, MANZANILLO, COLIMA.'),(66,'18','17','MATAMOROS, MATAMOROS, TAMAULIPAS.'),(67,'18','18','MAZATLAN, MAZATLAN, SINALOA.'),(68,'18','19','MEXICALI, MEXICALI, BAJA CALIFORNIA.'),(69,'18','20','MEXICO, DISTRITO FEDERAL.'),(70,'18','22','NACO, NACO, SONORA.'),(71,'18','23','NOGALES, NOGALES, SONORA.'),(72,'18','24','NUEVO LAREDO, NUEVO LAREDO, TAMAULIPAS.'),(73,'18','25','OJINAGA, OJINAGA, CHIHUAHUA.'),(74,'18','26','PUERTO PALOMAS, PUERTO PALOMAS, CHIHUAHUA.'),(75,'18','27','PIEDRAS NEGRAS, PIEDRAS NEGRAS, COAHUILA.'),(76,'18','28','PROGRESO, PROGRESO, YUCATAN.'),(77,'18','30','CIUDAD REYNOSA, CIUDAD REYNOSA, TAMAULIPAS.'),(78,'18','31','SALINA CRUZ, SALINA CRUZ, OAXACA.'),(79,'18','33','SAN LUIS RIO COLORADO, SAN LUIS RIO COLORADO, SONORA.'),(80,'18','34','CIUDAD MIGUEL ALEMAN, CIUDAD MIGUEL ALEMAN, TAMAULIPAS.'),(81,'18','37','CIUDAD HIDALGO, CIUDAD HIDALGO, CHIAPAS.'),(82,'18','38','TAMPICO, TAMPICO, TAMAULIPAS.'),(83,'18','39','TECATE, TECATE, BAJA CALIFORNIA.'),(84,'18','40','TIJUANA, TIJUANA, BAJA CALIFORNIA.'),(85,'18','42','TUXPAN, TUXPAN DE RODRIGUEZ CANO, VERACRUZ.'),(86,'18','43','VERACRUZ, VERACRUZ, VERACRUZ.'),(87,'18','44','CIUDAD ACUÑA, CIUDAD ACUÑA, COAHUILA.'),(88,'18','46','TORREON, TORREON, COAHUILA.'),(89,'18','47','AEROPUERTO INTERNACIONAL DE LA CIUDAD DE MEXICO,'),(90,'18','48','GUADALAJARA, TLACOMULCO DE ZUÑIGA, JALISCO.'),(91,'18','50','SONOYTA, SONOYTA, SONORA.'),(92,'18','51','LAZARO CARDENAS, LAZARO CARDENAS, MICHOACAN.'),(93,'18','52','MONTERREY, GENERAL MARIANO ESCOBEDO, NUEVO LEON.'),(94,'18','53','CANCUN, CANCUN, QUINTANA ROO.'),(95,'18','64','QUERÉTARO, EL MARQUÉS Y COLON, QUERÉTARO.'),(96,'18','65','TOLUCA, TOLUCA, ESTADO DE MEXICO.'),(97,'18','67','CHIHUAHUA, CHIHUAHUA, CHIHUAHUA.'),(98,'18','73','AGUASCALIENTES, AGUASCALIENTES, AGUASCALIENTES.'),(99,'18','75','PUEBLA, HEROICA PUEBLA DE ZARAGOZA, PUEBLA.'),(100,'18','80','COLOMBIA, COLOMBIA, NUEVO LEON.'),(101,'18','81','ALTAMIRA, ALTAMIRA, TAMAULIPAS.'),(102,'18','82','CIUDAD CAMARGO, CIUDAD CAMARGO, TAMAULIPAS.'),(103,'18','83','DOS BOCAS, PARAISO, TABASCO.'),(104,'18','84','GUANAJUATO, SILAO, GUANAJUATO.'),(105,'22','1','Enero'),(106,'22','2','Febrero'),(107,'22','3','Marzo'),(108,'22','4','Abril'),(109,'22','5','Mayo'),(110,'22','6','Junio'),(111,'22','7','Julio'),(112,'22','8','Agosto'),(113,'22','9','Septiembre'),(114,'22','10','Octubre'),(115,'22','11','Noviembre'),(116,'22','12','Diciembre'),(117,'30','C','Cliente'),(118,'30','E','Emisor'),(119,'31','PF','Persona Física'),(120,'31','PM','Persona Moral'),(121,'32','F','Facturación'),(122,'37','1','Comprobante emitido con errores con relación'),(123,'37','2','Comprobante emitido con errores sin relación'),(124,'37','3','No se llevó acabo la operación'),(125,'37','4','Operación nominativa relacionada en la factura'),(126,'30','PAC','Proveedor PAC'),(127,'13','H87','Pieza');
/*!40000 ALTER TABLE `clasificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `correo`
--

DROP TABLE IF EXISTS `correo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `correo` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `correo` varchar(100) DEFAULT NULL,
  `Idclasificacion` int DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `correo`
--

LOCK TABLES `correo` WRITE;
/*!40000 ALTER TABLE `correo` DISABLE KEYS */;
INSERT INTO `correo` VALUES (1,'facturas@hotmail.com',121),(2,'facturacion@easy-rez.com',121),(3,'correo',121),(4,'correo',121),(5,'correo',121),(6,'correo',121),(7,'correo',121);
/*!40000 ALTER TABLE `correo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpostal`
--

DROP TABLE IF EXISTS `cpostal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpostal` (
  `ID` int NOT NULL,
  `Cpostal` varchar(5) NOT NULL,
  `Municipio` varchar(45) NOT NULL,
  `Colonia` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpostal`
--

LOCK TABLES `cpostal` WRITE;
/*!40000 ALTER TABLE `cpostal` DISABLE KEYS */;
INSERT INTO `cpostal` VALUES (1,'04890','COYOACAN','JARDINES DE COYOACAN'),(2,'97280','MERIDA','5 COLONIAS'),(3,'09240','IZTAPALAPA','Progresista');
/*!40000 ALTER TABLE `cpostal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datosfacturacion`
--

DROP TABLE IF EXISTS `datosfacturacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `datosfacturacion` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `TipoPersona` int DEFAULT NULL,
  `RFC` varchar(45) DEFAULT NULL,
  `RazonSocial` varchar(100) DEFAULT NULL,
  `MetodoPago` int DEFAULT NULL,
  `USOCFDI` int DEFAULT NULL,
  `RegimenFiscal` int DEFAULT NULL,
  `Direccion` int DEFAULT NULL,
  `EsSucursal` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datosfacturacion`
--

LOCK TABLES `datosfacturacion` WRITE;
/*!40000 ALTER TABLE `datosfacturacion` DISABLE KEYS */;
INSERT INTO `datosfacturacion` VALUES (1,119,'XAXX010101000','Público en general',6,22,9,1,'No'),(2,119,'XEXX010101000','Público en general extranjero',6,22,9,1,'No'),(3,120,'ESO091210GK6‎','EZ SOLUTIONS S DE RL DE CV',6,22,13,1,'No'),(4,120,'RCH020621SP2','REACHCORE',6,22,13,1,'No'),(5,119,'RFCPrueba','Prueba Razon Social',6,22,9,1,'No'),(6,119,'RFCPrueba','Prueba Razon Social',6,22,9,1,'No'),(7,119,'RFCPrueba','Prueba Razon Social',6,22,9,1,'No'),(8,120,'pueba','paginaprueba',6,36,13,1,'No'),(9,119,'pueba','paginaprueba1',6,35,13,1,'No');
/*!40000 ALTER TABLE `datosfacturacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `direccion`
--

DROP TABLE IF EXISTS `direccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `direccion` (
  `iddireccion` int NOT NULL,
  `Linea1` varchar(45) DEFAULT NULL,
  `Linea2` varchar(45) DEFAULT NULL,
  `Referencia` varchar(45) DEFAULT NULL,
  `IdCpostal` varchar(45) DEFAULT NULL,
  `IdPais` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iddireccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direccion`
--

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;
INSERT INTO `direccion` VALUES (1,'Campanillas 42',NULL,NULL,'1','1');
/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entidadtributaria`
--

DROP TABLE IF EXISTS `entidadtributaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `entidadtributaria` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `IdClasificacion` int DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='			';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entidadtributaria`
--

LOCK TABLES `entidadtributaria` WRITE;
/*!40000 ALTER TABLE `entidadtributaria` DISABLE KEYS */;
INSERT INTO `entidadtributaria` VALUES (1,118),(2,117),(3,126),(4,126),(5,126),(6,126),(7,126),(8,117);
/*!40000 ALTER TABLE `entidadtributaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enumtexto`
--

DROP TABLE IF EXISTS `enumtexto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enumtexto` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `grupo` varchar(10) DEFAULT NULL,
  `valor` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enumtexto`
--

LOCK TABLES `enumtexto` WRITE;
/*!40000 ALTER TABLE `enumtexto` DISABLE KEYS */;
INSERT INTO `enumtexto` VALUES (1,'2','No'),(2,'2','Opcional'),(3,'2','[0-9]{11}|[0-9]{18}'),(4,'2','[0-9]{10,11}|[0-9]{15,16}|[0-9]{18}|[A-Z0-9_]{10,50}'),(5,'2','[0-9]{10}|[0-9]{16}|[0-9]{18}'),(6,'2','[0-9]{16}'),(7,'2','[0-9]{10}'),(8,'2','[0-9]{15,16}'),(9,'2','Si'),(10,'3','Manual'),(11,'3','Automático'),(12,'9','RegimenFiscal'),(13,'9','Uso CFDI'),(14,'10','Fisica'),(15,'10','Moral'),(16,'10','Fisica-Moral'),(17,'11','IVA'),(18,'11','IEPS'),(19,'11','IVA-IEPS'),(20,'11','IVA-IEPS Opcional'),(21,'12','Descripcion'),(22,'12','Nota'),(23,'15','Traslado'),(24,'15','Retencion'),(25,'15','Traslado-Retencion'),(26,'17','Tasa'),(27,'17','Cuota'),(28,'17','Exento'),(29,'19','0'),(30,'19','74'),(31,'19','101'),(32,'19','115'),(33,'19','122'),(34,'19','123'),(35,'19','149'),(36,'19','150'),(37,'19','182'),(38,'19','188'),(39,'19','203'),(40,'19','205'),(41,'19','227');
/*!40000 ALTER TABLE `enumtexto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pais` (
  `ID` int NOT NULL,
  `PAIS` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'MEXICO');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `redfcorreos`
--

DROP TABLE IF EXISTS `redfcorreos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `redfcorreos` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `IdDatosF` int DEFAULT NULL,
  `IDCorreo` int DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `redfcorreos`
--

LOCK TABLES `redfcorreos` WRITE;
/*!40000 ALTER TABLE `redfcorreos` DISABLE KEYS */;
INSERT INTO `redfcorreos` VALUES (1,1,1),(2,2,1),(3,3,2),(4,4,2),(5,5,3),(6,6,4),(7,7,5),(8,8,6),(9,9,7);
/*!40000 ALTER TABLE `redfcorreos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reetdf`
--

DROP TABLE IF EXISTS `reetdf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reetdf` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `IDEntidad` int DEFAULT NULL,
  `IDDatosF` int DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reetdf`
--

LOCK TABLES `reetdf` WRITE;
/*!40000 ALTER TABLE `reetdf` DISABLE KEYS */;
INSERT INTO `reetdf` VALUES (1,2,3),(2,2,1),(3,1,4),(4,3,2),(5,6,7),(6,7,8),(7,8,9);
/*!40000 ALTER TABLE `reetdf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'roberto'
--
/*!50003 DROP PROCEDURE IF EXISTS `buscarregimen` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarregimen`()
BEGIN
select * from roberto.artefactocfdi where idtipo=12;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `buscartipopersona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscartipopersona`()
BEGIN
SELECT * FROM roberto.clasificacion where grupo=31;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `busquedaentidad` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `busquedaentidad`()
BEGIN
SELECT * FROM roberto.clasificacion where grupo=30 ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `BusquedaEntidadFinanciera` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `BusquedaEntidadFinanciera`(in TipoEntidad int)
BEGIN
Select ClasificacionEntidad.Nombre as "TipoEntidad",clasificacion.Nombre as "TipoPersona",datosfacturacion.RazonSocial,datosfacturacion.RFC,correo.correo,MetodoPago.Nombre as "MetodoPago"
,artefactocfdi.Clave as "USOCFEDICLAVE",artefactocfdi.Descripcion as "USOCFDIDES",enumtexto.valor AS "TipoPersona",Regimen.Clave as "RegimenClave",Regimen.Descripcion as "RegimenDes" from roberto.reetdf inner join roberto.datosfacturacion on roberto.reetdf.IDDatosF = roberto.datosfacturacion.id
inner join clasificacion on datosfacturacion.TipoPersona=clasificacion.id
inner join clasificacion As MetodoPago on MetodoPago.id=datosfacturacion.MetodoPago
Inner join roberto.artefactocfdi on datosfacturacion.USOCFDI=roberto.artefactocfdi.id
inner join roberto.artefactocfdi as `Regimen` on roberto.datosfacturacion.RegimenFiscal=roberto.Regimen.ID
inner join roberto.redfcorreos on datosfacturacion.id=roberto.redfcorreos.IdDatosF
inner join correo on roberto.redfcorreos.IDCorreo=correo.ID
inner join entidadtributaria on entidadtributaria.ID=roberto.reetdf.IDEntidad
inner join clasificacion as `ClasificacionEntidad`on entidadtributaria.IdClasificacion=ClasificacionEntidad.ID
inner join enumtexto on artefactocfdi.AplicaciontipoPersona=enumtexto.ID
where ClasificacionEntidad.ID=TipoEntidad;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cfdibusqueda` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cfdibusqueda`()
BEGIN
select * from roberto.artefactocfdi where idtipo=13;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CrearEntidadTributaria` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `CrearEntidadTributaria`(Correo varchar(50),tipopersona int,rfc varchar(45),razonsocial varchar(100),metodopago int,uocfdi int,regimen int,sucursal varchar(45),entidadt int)
BEGIN
INSERT INTO `roberto`.`correo`(`correo`,`Idclasificacion`)VALUES('correo',121);
SET @correoid = LAST_INSERT_ID();
INSERT INTO `roberto`.`datosfacturacion`(`TipoPersona`,`RFC`,`MetodoPago`,`USOCFDI`,`RazonSocial`,`RegimenFiscal`,`Direccion`,`EsSucursal`)VALUES(tipopersona,rfc,metodopago,uocfdi,razonsocial,regimen,1,sucursal);
SET @iddf = LAST_INSERT_ID();
INSERT INTO `roberto`.`redfcorreos`(`IdDatosF`,`IDCorreo`)VALUES(@iddf,@correoid);
INSERT INTO `roberto`.`entidadtributaria` (`IdClasificacion`)VALUES(entidadt);
SET @TipoEntidad = LAST_INSERT_ID();
INSERT INTO `roberto`.`reetdf`(`IDEntidad`,`IDDatosF`)VALUES(@TipoEntidad,@iddf);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-04  9:57:20
